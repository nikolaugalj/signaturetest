package com.velimirovixxx.signaturetestproject

import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import androidx.appcompat.app.AppCompatActivity
import com.goterl.lazycode.lazysodium.LazySodiumAndroid
import com.goterl.lazycode.lazysodium.SodiumAndroid
import com.goterl.lazycode.lazysodium.utils.Key
import org.bouncycastle.asn1.DERBitString
import org.bouncycastle.asn1.DERSequence
import org.bouncycastle.jce.provider.BouncyCastleProvider
import timber.log.Timber
import java.security.*

class MainActivity : AppCompatActivity() {
    private var lazySodium = LazySodiumAndroid(SodiumAndroid())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val provider = BouncyCastleProvider()
        Security.addProvider(provider)

        val cryptoSignKeypair = lazySodium.cryptoSignKeypair()
        Timber.plant(Timber.DebugTree())
        val kpg: KeyPairGenerator = KeyPairGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_EC, ANDROID_KEYSTORE
        )

        val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
            SMT_ALIAS,
            KeyProperties.PURPOSE_SIGN or KeyProperties.PURPOSE_VERIFY or KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).run {

//            setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
//            setRandomizedEncryptionRequired(true)
//            setAlgorithmParameterSpec(ECGenParameterSpec("secp256r1"))//Supported: [p-224, p-256, p-384, p-521, prime256v1, secp224r1, secp256r1, secp384r1, secp521r1]
            setDigests(KeyProperties.DIGEST_NONE)
//            setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
            build()
        }
        kpg.initialize(parameterSpec)

        val pair = kpg.generateKeyPair()
        val priv = pair.private
        val pub = pair.public

        //decoding from der sequence
        val sequence = DERSequence.getInstance(pub.encoded)
        val subjectPublicKey = sequence.getObjectAt(1) as DERBitString
        val subjectPublicKeyBytes = Base64.encode(subjectPublicKey.bytes, Base64.NO_WRAP)

        Timber.d("pub1: ${String(subjectPublicKeyBytes)} length: ${subjectPublicKeyBytes.size}")
        Timber.d("pub2: ${cryptoSignKeypair.publicKey.asBytes} length: ${cryptoSignKeypair.publicKey.asBytes.size}")

        val data = "yDjSaL0tpxu8Oc1DJVsiFP5pVFpFmlKc"
        val ks = KeyStore.getInstance(ANDROID_KEYSTORE).apply {
            load(null)
        }
        val publicKey: PublicKey = ks.getCertificate(SMT_ALIAS).publicKey
        val entry: KeyStore.Entry = ks.getEntry(SMT_ALIAS, null)
        if (entry is KeyStore.PrivateKeyEntry) {
            val signature: ByteArray = Signature.getInstance(ALGORITHM).run {
                initSign(entry.privateKey)
                update(data.toByteArray())
                sign()
            }
            val opened = KeystoreRepository3.open(signature, Key.fromBytes(subjectPublicKey.bytes))
            val base64 = String(Base64.encode(signature, Base64.NO_WRAP))
            Timber.d("signature: $signature base64: $base64")
            Timber.d("opened: $opened")
        }
    }


    companion object {
        private const val ANDROID_KEYSTORE = "AndroidKeyStore"
        private const val SMT_ALIAS = "NIKOLA"
        private const val ALGORITHM = "NONEwithECDSA"
    }
}
