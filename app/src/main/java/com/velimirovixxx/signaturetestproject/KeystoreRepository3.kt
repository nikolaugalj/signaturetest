package com.velimirovixxx.signaturetestproject

import android.util.Base64
import com.goterl.lazycode.lazysodium.LazySodiumAndroid
import com.goterl.lazycode.lazysodium.SodiumAndroid
import com.goterl.lazycode.lazysodium.exceptions.SodiumException
import com.goterl.lazycode.lazysodium.interfaces.Sign
import com.goterl.lazycode.lazysodium.utils.Key

object KeystoreRepository3 {

    private var lazySodium = LazySodiumAndroid(SodiumAndroid())
    private val keyPair = lazySodium.cryptoSignKeypair()

    fun getPublicKey() = Base64.encodeToString(keyPair.publicKey.asBytes, Base64.NO_WRAP)
    fun getPrivateKey() = Base64.encodeToString(keyPair.secretKey.asBytes, Base64.NO_WRAP)

    fun sign(text: String, secretKeyBytes: ByteArray = keyPair.secretKey.asBytes): String {
        val messageBytes: ByteArray = lazySodium.bytes(text)
        val signedMessage: ByteArray = lazySodium.randomBytesBuf(Sign.BYTES + messageBytes.size)
        val res: Boolean = lazySodium.cryptoSign(
            signedMessage,
            messageBytes,
            messageBytes.size.toLong(),
            secretKeyBytes
        )
        if (!res) {
            throw SodiumException("Could not sign your message.")
        }
        return Base64.encodeToString(signedMessage, Base64.NO_WRAP)
    }

    fun open(signedMessageBytes: ByteArray, publicKey: Key): String? {
        val publicKeyBytes = publicKey.asBytes
        val messageBytes: ByteArray =
            lazySodium.randomBytesBuf(signedMessageBytes.size - Sign.BYTES)

        val res: Boolean = lazySodium.cryptoSignOpen(
            messageBytes,
            signedMessageBytes,
            signedMessageBytes.size.toLong(),
            publicKeyBytes
        )

        return if (!res) {
            null
        } else lazySodium.str(messageBytes)
    }

}